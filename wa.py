#!/usr/local/bin/python3

import wolframalpha
import sys


def main():
    if len(sys.argv) > 1:
        query = " ".join(sys.argv[1:])
    else:
        query = input('Enter query: ')

    client = wolframalpha.Client("63JQWV-QLLV2P7TY8")
    res = client.query(query)

    # Unable to resolve question
    if res['@success'] == 'false':
        print('Question Unresolved! ')
    # question resloved
    else:
        pod1 = res['pod'][1]
        result = resolveListOrDict(pod1['subpod'])
        print(result)

        for pod in res.pods:
            print(pod['@title'])

          

def resolveListOrDict(var):
    if isinstance(var, list):
        return var[0]['plaintext']
    else:
        return var['plaintext']

if __name__ == '__main__':
    main()
